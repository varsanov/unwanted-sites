#! /usr/bin/env ruby

require "test/unit"
require_relative 'src/rc/nameholder'

=begin
Test::Unit::AutoRunner.class.module_eval {
    def need_auto_run?
        true
    end }
=end

#a.class.module_eval { attr_accessor :need_auto_run?}

class ProjectTest < Test::Unit::TestCase
    def test_wildcards
        name_holder = NameHolder.new
        name_holder.add_item("*mail.ru".reverse!, "255.255.255.255")
        name_holder.add_item("*.yandex.ru".reverse!, "255.255.255.255")
        ya_ip = name_holder.get_item("www.yandex.ru".reverse!)
        mail_ip = name_holder.get_item("mail.ru".reverse!)
        assert_equal("255.255.255.255", ya_ip)
        assert_equal("255.255.255.255", mail_ip)
        ya_ip = name_holder.get_item("yandex.ru".reverse!)
        mail_ip = name_holder.get_item("top.mail.ru".reverse!)
        assert_equal(nil, ya_ip)
        assert_equal("255.255.255.255", mail_ip)

        name_holder.add_item("*yandex.ru".reverse!, "255.255.255.255")
        ya_ip = name_holder.get_item(".yandex.ru".reverse!)
        assert_equal("255.255.255.255", ya_ip)
        ya_ip = name_holder.get_item("yandex.ru".reverse!)
        assert_equal("255.255.255.255", ya_ip)

	name_holder.add_item("*.top.mail.ru".reverse!, "255.255.255.255")
	top_mail = name_holder.get_item("top.mail.ru".reverse!)
	assert_equal(nil, top_mail)
	top_mail = name_holder.get_item("d1.c3.b2.a2.top.mail.ru".reverse!)
	assert_equal("255.255.255.255", top_mail)
    end

end
