#! /usr/bin/env ruby


class NameHolder
    attr_reader :value
    def initialize
        @child = Hash.new
    end

    def add_item(word, value)
        if word.size > 0
            if not @child[word[0]]
                @child[word[0]] = NameHolder.new
            end
            temp = word[0]
            word[0] = ''
            @child[temp].add_item(word, value)
        else
            @value = value;
        end
    end

    def get_item(word)

        if word.size == 0
            if !@value and @child['*']
                return @child['*'].value
            end
            return @value
        end

        temp = word[0]
        word[0] = ''
        if @child[temp]
            return @child[temp].get_item(word)
        elsif @child['*']
            return @child['*'].value
        else
            return nil
        end


    end
end