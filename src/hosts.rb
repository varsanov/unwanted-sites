#! /usr/bin/env ruby

require_relative 'nameholder'

class Hosts
  def initialize(path)
    @path = path
    @nameHolder = NameHolder.new
  end

  def load
    file = File.new(@path, 'r')
    while (line = file.gets)
      line.scan(/^\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+(\S+)\s*$/) do |a, b|

        @nameHolder.add_item b.reverse!, a
      end
    end
    file.close
  end

  def query(name)
    p @nameHolder.get_item name.reverse!
  end
end



