#!/usr/bin/env ruby
require 'async/dns'

require 'logger'

require_relative 'hosts'

require_relative 'settings'

IN = Resolv::DNS::Resource::IN

Name = Resolv::DNS::Name


class DnsServer < Async::DNS::Server

  def load_settings(parameter_list)
    if File.file?('settings.yml')
      Settings.load!('settings.yml')
    elsif File.file?('/etc/dns_adblock/settings.yml')
      Settings.load!('/etc/dns_adblock/settings.yml')
    elsif File.file?('~/dns_adblock/settings.yml')
      Settings.load!('~/dns_adblock/settings.yml')
    end
  end

  def initialize1(parameter_list)

    @logger = Logger.new(STDOUT)
    @logger.info "Service started"

    load_settings([])

    @CACHE = {}


    @CACHE_TIME = 60 * Settings.main["cache_timeout"]

    @INTERFACES = [
        [:udp, Settings.main["listen_address"], Settings.main["listen_port"]],
        [:tcp, Settings.main["listen_address"], Settings.main["listen_port"]]
    ]

    @hosts = Hosts.new(Settings.main["hosts_path"])
    @hosts.load


  end

  def process(name, resource_class, transaction)
    key = [transaction.name, transaction.resource_class]
    cache = @CACHE[key]

    if cache and (Time.now - cache[1]) < @CACHE_TIME
      @logger.info "Cached: #{transaction.name}..."
      transaction.respond!(cache[0])
    else
      resolver = Async::DNS::Resolver.new([[:udp, Settings.main["resolve_address"], Settings.main["resolve_port"]],
                                           [:tcp, Settings.main["resolve_address"], Settings.main["resolve_port"]]])
      temp = @hosts.query transaction.name
      if temp
        @logger.info "Found item in the blacklist: #{transaction.question.to_s}"
        transaction.respond!(temp)
        @CACHE[key] = [temp, Time.now]
      else
        @logger.info "Lookup: #{transaction.question.to_s}"
        transaction.passthrough!(resolver) do |reply, reply_name|
          @logger.info reply
          @logger.info "\n\n\n\n"
          @logger.info reply_name
          @logger.info "\n\n\n\n";
          answer = reply.answer[0][2]
          if answer.class == IN::A
            @CACHE[key] = [answer.address.to_s, Time.now]
          end
        end
      end
    end


  end

end